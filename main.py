#-*-coding:utf-8-*-
__author__ = 'zaikin lynch0000@gmail.com'

from PyQt4 import QtCore, QtGui, uic
from functools import partial
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import string
import time
import json
import binascii
import math
import pyqtgraph as pg
del bin
del hex
import colorama
import numpy as np
colorama.init(strip=False)
from colorama import Fore, Back, Style
import struct
import socket
print(Fore.LIGHTMAGENTA_EX)

UDP_IP = "10.0.0.1"
UDP_PORT = 8888
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

import struct
import math
import numpy as np
udp_frame_length = 1464.0
BUFLEN = 1514
HEADER = [0x33,0x22,0x33,0x33]
class MyWindow(QtGui.QMainWindow):
    def __init__(self, parent = None):
        QtGui.QTabWidget.__init__(self, parent)
        uic.loadUi("./bco_stream_face.ui", self)
        self.setWindowTitle(u'bco stream v0.1    28.07.2021')
        self.FadcHz = 1e9
        self.xrange = 20000
        xdict = dict(enumerate(range(self.xrange)))
        stringaxis = pg.AxisItem(orientation='bottom')
        stringaxis.setTicks([xdict.items()])
        self.pw = self.graphicsView.addPlot(axisItems={'bottom': stringaxis})
        self.graphicsView.setBackground("333333")
        self.pw.addLegend(offset=(300, -20))
        self.pw.setXRange(0, 20 + 10)
        self.pw.setYRange(-30000, 30000)
        self.pw.setLabels(bottom='Time, usec')
        self.pw.showGrid(x=True, y=True, alpha=1)
        self.mainplot = self.pw.plot()
        self.rec_button.clicked.connect(self.startreceiveThread)
    def startreceiveThread(self):
        print "preparing receiving thread"
        self.rc = receiverClass(self)
        self.thread = QThread()
        self.rc.moveToThread(self.thread)
        self.thread.started.connect(self.rc.receiveRun)
        self.rc.finished.connect(self.thread.quit)
        self.rc.finished.connect(self.rc.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.rc.received.connect(self.drawFrames)
        self.rc.run = True
        self.thread.start()
        self.thread.finished.connect(self.finishedHndlr)

    def drawFrames(self, drawable, leng2, nzeros):
        #print "drawFrames"
        #print 'drawable:', drawable
        #print "len(drawable):",len(drawable)
        #print "leng2:", leng2
        #print "nzeros:", nzeros
        Nchannels = 1 # todo do
        Nbytes2 = leng2 - nzeros - 512
        #print "Nbytes2:",Nbytes2
        drawable  =drawable[120+512:]
        if Nbytes2 < len(drawable):
            drawable = drawable[:-(len(drawable)-Nbytes2)]
        #print "len(drawable):", len(drawable)
        N1 = Nbytes2 / (2 * Nchannels) # otschetov
        decimation = 0
        kddc = 1
        h = (decimation+1)*1e6/self.FadcHz
        X1 = []
        #print "N1:", N1
        for i in range(N1):
            X1.append(kddc * i* h)
        i = 0
        temp = drawable
        temp = drawable#[120+512:]
        #temp = temp[:-nzeros]
        temp = temp
        #print "len(temp):",len(temp)
        #print "temp:",temp
        temp2=''
        for q in temp:
            temp2+=q
        Y1 = struct.unpack(N1*'h',temp2)
        #print X1
        self.mainplot.setData(y=Y1,x=X1)

    def finishedHndlr(self):
        print "receiv thread finished"

class receiverClass(QObject):
    finished = pyqtSignal()
    received = pyqtSignal(list, int, int)
    def __init__(self, parent=None):
        super(receiverClass, self).__init__()
        self.FadcHz = 1e9

    def receiveRun(self):

        #leng = fread(file, 1, 'uint32')  # ; % длина
        #leng = (packet_info.len - header_size) / 2  # ;
        #recv_frame = np.array(1000)
        #ydata = np.array(struct.unpack('h' * leng, f.read(2 * leng)))
        #xdata = map(lambda x: x * (1.e6 / self.FadcHz), range(leng))
        while (self.run):
            data = sock.recv(BUFLEN)  # buffer size is 1024 bytes
            if list(data[:4]) == map(chr, HEADER):
                #print 'head'
                leng = struct.unpack("I", data[4:8])[0]
                num =  struct.unpack("I", data[8 + 4:8 + 4 + 4])[0]
                num2 = struct.unpack("I", data[8 + 4 + 4:8 + 4 + 4 + 4])[0]
                #print 'leng:', leng
                #print "num:",num
                #print "num2:", num2
                npacketscalced = int(math.ceil(leng / udp_frame_length))
                #print"npacketscalced:", npacketscalced
                nzeros = int(npacketscalced * udp_frame_length - leng)
                #print "nzeros:", nzeros - leng
                buflen = int(npacketscalced * udp_frame_length)
                #print "buflen:", buflen
                tempdata = ''
                tempdata += data
                #print data
                for j in xrange(npacketscalced - 1):
                    data2 = sock.recv(BUFLEN)
                    tempdata += data2
                    #print j
                #print 'len(tempdata):', len(tempdata)

                if (not num %300):self.received.emit(list(tempdata), leng, nzeros)
                #if (not num %300):self.emit(SIGNAL("received"),tempdata, leng, nzeros)
                #self.run = 0
                #return
                #tempdata = struct.unpack((buflen - 120 - 512) * "h", tempdata)

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())