﻿from PyQt5 import QtCore, QtGui, QtWidgets, uic
from functools import partial
from PyQt5.QtCore import pyqtSignal, QObject, QThread
from PyQt5.QtWidgets import QMainWindow, QApplication, QTabWidget
import string
import time
import json
import binascii
import math
import pyqtgraph as pg

import colorama
import numpy as np
colorama.init(strip=False)
from colorama import Fore, Back, Style
import os
import struct
import socket
print(Fore.LIGHTMAGENTA_EX)
udp_frame_length = 1464.0

BUFLEN = 1514
HEADER = [0x33, 0x22, 0x33, 0x33]

from dataProvider import dataProvider


class receiverClass(QObject):
    finished = pyqtSignal()
    received = pyqtSignal(list, int, int, int, int)
    def __init__(self, parent=None):
        super(receiverClass, self).__init__()
        self.FadcHz = 1e9
        self.parent = parent
        self.fromFile = False
        self.filep = None
        self.sockp = None
        self.d = dataProvider()
        self.WeChooseReadFromFile = False
        self.pathFile=''

    def closeFileSocket(self):
        self.d.closeFile()
        self.d.closeSocket()

    def receiveRun(self):
        prg_bar=0

        if self.WeChooseReadFromFile == False:
            self.d.useSocket()
            print("READ SOCKET")
        else:
            self.d.filename = self.pathFile
            self.d.useFile()

            print("READ FILE")

        while (self.run):
            #print('data = sock.recv')
            if self.d.CHUNK1464:
                data = self.d.getChunk()
            else:
                data = self.d.getChunk()[:-4]

            #print(data)
            #data = f.read(CHUNK_SIZE)
            #print(map(chr, data[:4]))
            if data[:4] == bytearray(HEADER):
                #print('head at:',f.tell())
                leng = struct.unpack("I", data[4:8])[0]
                #print("leng:",leng)
                num =  struct.unpack("I", data[8 + 4:8 + 4 + 4])[0]
                num2 = struct.unpack("I", data[8 + 4 + 4:8 + 4 + 4 + 4])[0]

                prg_bar = self.d.prg_bar ##прогресс выполнения
                    #print('progress is:', prg_bar)

                channels = (struct.unpack("c", data[99:100]))[0] #c - char 1byte
                channels = int.from_bytes(channels, byteorder='big')

                Nchannels = 0

                if channels & 0b0001:
                    Nchannels += 1
                if channels & 0b0010:
                    Nchannels += 1
                if channels & 0b0100:
                    Nchannels += 1
                if channels & 0b1000:
                    Nchannels += 1

                #print("number of channels: ", Nchannels)

                #print 'leng:', leng
                #print "num:",num
                #print "num2:", num2
                npacketscalced = int(math.ceil(leng / udp_frame_length))
                #print"npacketscalced:", npacketscalced
                nzeros = int(npacketscalced * udp_frame_length - leng)
                #print "nzeros:", nzeros - leng
                buflen = int(npacketscalced * udp_frame_length)
                #print "buflen:", buflen
                tempdata = []
                #print(data)
                tempdata += bytearray(data)
                for j in range(npacketscalced - 1):
                    if self.d.CHUNK1464:
                        data2 = self.d.getChunk()
                    else:
                        data2 = self.d.getChunk()[:-4]
                    if data2[:4] == bytearray(HEADER):
                        print("DROP!")
                    tempdata += bytearray(data2)
                    #print(j)
                #print ('len(tempdata):', len(tempdata))
                #print(list(tempdata))

                if not num % 300:
                    self.received.emit(list(tempdata), leng, nzeros, Nchannels, prg_bar)
