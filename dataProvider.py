﻿from PyQt5 import QtCore, QtGui, QtWidgets, uic
from functools import partial
from PyQt5.QtCore import pyqtSignal, QObject, QThread
from PyQt5.QtWidgets import QMainWindow, QApplication, QTabWidget
import string
import time
import json
import binascii
import math
import pyqtgraph as pg

import colorama
import numpy as np
colorama.init(strip=False)
from colorama import Fore, Back, Style
import os
import struct
import socket
CHUNK_SIZE = 1468
FILE_CHNUNK_SIZE = 1464
UDP_IP = "10.0.0.1"
UDP_PORT = 8888

class dataProvider(QObject):
    def __init__(self, parent=None, filename = ''):
        super(dataProvider, self).__init__()
        self.parent = parent
        self.filename = filename
        self.usefile = False
        self.firstPacket = True
        self.CHUNK1464 = True
        self.prg_bar = 0
        #self.openSocket()

    def openFile(self):
        self.f = open(self.filename,'rb')
        self.sizeFile = int(os.path.getsize(self.filename))
        print(self.sizeFile)

    def openSocket(self):
        self.s = socket.socket(socket.AF_INET, # Internet
                               socket.SOCK_DGRAM) # UDP
        self.s.bind((UDP_IP, UDP_PORT))

    def useFile(self):
        self.usefile = True
        #self.closeSocket()
        self.openFile()


    def useSocket(self):
        self.usefile = False
        #self.closeFile()
        self.openSocket()

    def getChunk(self):
        if self.usefile:
            data = self.f.read(FILE_CHNUNK_SIZE)
            self.prg_bar = int(int(self.f.tell()) / self.sizeFile * 100)
            #print(int(self.f.tell()))
            if not data:
                self.f.seek(0)
                data = self.f.read(FILE_CHNUNK_SIZE)
            return data
        else:
            if not self.firstPacket:
                if self.s.recv(CHUNK_SIZE):
                    return self.s.recv(CHUNK_SIZE)
            else:
                self.firstPacketReceive()
                return self.s.recv(CHUNK_SIZE)

    def closeFile(self):
        self.f.close()
    def closeSocket(self):
        self.s.close()

    def firstPacketReceive(self):
        s = self.s.recv(2000)
        if len(s) == 1468:
            self.CHUNK1464 = False
        if len(s) == 1464:
            self.CHUNK1464 = True
        self.firstPacket = False