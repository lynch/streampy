from scipy.fft import fft, fftfreq
import numpy as np


class fftCalc():
    def __init__(self):
        self.numberOfChannels = 1
        self.FFTout = []

    def dataToFFT(self, NumOfCh, Samples, X, Y1, Y2=0, Y3=0, Y4=0, windowFFT=False):
        Data = [[],[],[],[]]
        Data[0] = Y1
        Data[1] = Y2
        Data[2] = Y3
        Data[3] = Y4

        if not NumOfCh:
            for i in range(NumOfCh):
                self.FFTout[i] = self.OneChanneLFFT(Samples, Data[i], X, windowFFT)

        else:
            return np.array([0]), np.array([0])

        return self.FFTout


    def OneChannelFFT(self, length=0, Y=0, X=0, windowFFT=False, log10view=True):

        N = length
        print('Number of sample points - {0}'.format(N))
        # sample spacing
        T = 1.0 / 1000
        x = np.array(X)
        y = np.array(Y)

        if N:
            if windowFFT == True:
                from scipy.signal import blackman
                w = blackman(N)
                yf = 2.0 / N * np.abs(fft(y * w)[0:N // 2])
            else:
                yf = 2.0 / N * np.abs(fft(y)[0:N // 2])

            if log10view:
                yf = np.log10(yf)

            xf = fftfreq(N, T)[:N // 2]
            return [list(yf), list(xf)]
        else:
            return 0


