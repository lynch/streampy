﻿
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from functools import partial
from PyQt5.QtCore import pyqtSignal, QObject, QThread
from PyQt5.QtWidgets import QMainWindow, QApplication, QTabWidget
import string
import time
import json
import binascii
import math
import pyqtgraph as pg

import colorama
import numpy as np
colorama.init(strip=False)
from colorama import Fore, Back, Style
import os
import struct
import socket
print(Fore.LIGHTMAGENTA_EX)
from receiverClass import receiverClass

from dataProvider import dataProvider
from start_stream import InitBSCO

from fftCalc import fftCalc

class MyWindow(QMainWindow):
    def __init__(self, parent = None):
        QTabWidget.__init__(self, parent)
        uic.loadUi("./bco_stream_face.ui", self)
        self.setWindowTitle(u'bco stream v0.1    28.07.2021')
        self.FadcHz = 1e9
        self.xrange = 100
        xdict = dict(enumerate(range(self.xrange)))
        stringaxis = pg.AxisItem(orientation='bottom')
        stringaxis.setTicks([xdict.items()])
        self.pw = self.graphicsView.addPlot(axisItems={'bottom': stringaxis})
        self.graphicsView.setBackground("333333")
        self.pw.addLegend(offset=(300, -20))
        self.pw.setXRange(0, (20 + 10))  #self.pw.setXRange(0, 20 + 10)
        self.pw.setYRange(-30000, 30000)
        self.pw.setLabels(bottom='Time, nsec')
        self.pw.showGrid(x=True, y=True, alpha=1)
        self.mainplot1 = self.pw.plot(pen=(50,255,255))
        self.mainplot2 = self.pw.plot(pen=(255,55,255))
        self.mainplot3 = self.pw.plot(pen=(255,255,55))
        self.mainplot4 = self.pw.plot(pen=(50,55,255))

        #FFT block
        self.fftrange = 1000
        fftdict = dict(enumerate(range(self.fftrange)))
        stringaxis1 = pg.AxisItem(orientation='bottom')
        stringaxis1.setTicks([fftdict.items()])
        self.fft_draw = self.graphicsView_2.addPlot()#axisItems={'bottom': stringaxis1})
        self.graphicsView_2.setBackground("333333")
        self.fft_draw.addLegend(offset=(300, -20))
        self.fft_draw.setXRange(0, 1000)  # self.pw.setXRange(0, 20 + 10)
        self.fft_draw.setYRange(0, 30)
        self.fft_draw.setLabels(bottom='Freq, MHz')
        self.fft_draw.showGrid(x=True, y=True, alpha=1)
        self.fftplot1 = self.fft_draw.plot(pen=(50, 255, 255))
        self.fftplot2 = self.fft_draw.plot(pen=(255, 55, 255))
        self.fftplot3 = self.fft_draw.plot(pen=(255, 255, 55))
        self.fftplot4 = self.fft_draw.plot(pen=(50, 55, 255))

        self.file_toolButton.clicked.connect(self.browse_folder)

        self.ongoingReceiveFlag = False
        self.rec_button.clicked.connect(self.startreceiveThread)
        self.stop_button.clicked.connect(self.stopreceiveThread)
        self.pathFile = ''

        self.radioButton_file.toggled.connect(self.radioOnClicked)
        self.radioButton_socket.toggled.connect(self.radioOnClicked)
        self.WeChooseReadFromFile = True

        self.pushButton_startBSCO.clicked.connect(self.startBSCOinit)

    def startBSCOinit(self):
        self.BSCO = InitBSCO
        self.BSCO.start()

    def radioOnClicked(self):
        try:
            self.stopreceiveThread()
        except Exception as e:
            print(e)

        if self.radioButton_file.isChecked():            
            self.WeChooseReadFromFile = True
        else:
            self.WeChooseReadFromFile = False


    def browse_folder(self): #choosing file
        self.pathFile, _ = QtWidgets.QFileDialog.getOpenFileName(
                                                    self,
                                                    'Open File', './',
                                                    'Files (*.bin)')
        if self.pathFile:
            self.file_lineEdit.setText(self.pathFile)
        else:
            print(f'no')
            msg = QtWidgets.QMessageBox.information(self, 'Message', 'Вы ничего не выбрали.')

    def stopreceiveThread(self):
        self.rc.run = False
        self.thread.quit()

    def startreceiveThread(self):
        try:
            if not self.rc.run:
                print("preparing receiving thread")
            else:
                print("stop first")
                return
        except:
            pass

        self.rc = receiverClass(self)
        self.rc.WeChooseReadFromFile = self.WeChooseReadFromFile
        self.rc.pathFile = self.pathFile
        self.thread = QThread()
        self.rc.moveToThread(self.thread)
        self.thread.started.connect(self.rc.receiveRun)
        self.rc.finished.connect(self.thread.quit)
        self.rc.finished.connect(self.rc.deleteLater)
        self.rc.finished.connect(self.rc.closeFileSocket)

        self.thread.finished.connect(self.thread.deleteLater)
        self.rc.received.connect(self.drawFrames)
        self.rc.run = True
        self.thread.start()
        self.thread.finished.connect(self.finishedHndlr)

    def drawFrames(self, drawable, leng2, nzeros, Nchannels, prg_bar):

        #print("prg_bar:", prg_bar)
        self.Play_progressBar.setValue(prg_bar)
        #print "drawFrames"
        #print 'drawable:', drawable
        #print "len(drawable):",len(drawable)
        #print "leng2:", leng2
        #print("nzeros:", nzeros)
        #Nchannels = 1 # todo do
        Nbytes2 = leng2 - nzeros - 512
        #print "Nbytes2:",Nbytes2
        drawable  =drawable[120+512:]
        for i in range(5):
            drawable[i] = 0
        if Nbytes2 < len(drawable):
            drawable = drawable[:-(len(drawable)-Nbytes2)]
        #print "len(drawable):", len(drawable)
        N1 = int(Nbytes2 / (2 * Nchannels)) # otschetov
        decimation = 0
        kddc = 1
        h = (decimation+1)*1e6/self.FadcHz
        X1 = []
        #print("N1:", N1)


        #temp = drawable
        temp = drawable#[120+512:]
        #temp = temp[:-nzeros]
        temp = bytes(temp)
        #print "len(temp):",len(temp)
        #print("type(temp:",type(temp[0]))
        #temp2=''
        #for q in temp:
        #    temp2+=ord(q)
        #temp2=bytearray(temp2)
        h16temp = list(struct.unpack(int(len(temp) / 2)*'h',temp)) #int(len(temp) / 2) - number of 2byte objects

        Y1=[]
        Y2=[]
        Y3=[]
        Y4=[]

        #unpack 1, 2, 3 or 4 signal's Y from thread
        if Nchannels == 3:
            Divider3or4 = 12
        else:
            Divider3or4 = 16

        for i in range(int(len(h16temp) / Divider3or4)): # 4 channels = 4*4 2byte(h) numbers, 3ch = 3*4 2b (h) nums
            i12 = i * 12
            i16 = i * 16
            if Nchannels == 3:
                Y1.extend(h16temp[int(0 + i12):int(4 + i12)])
                Y2.extend(h16temp[int(4 + i12):int(8 + i12)])
                Y3.extend(h16temp[int(8 + i12):int(12 + i12)])
            if Nchannels == 1:
                Y1.extend(h16temp[int(0 + i16):int(16 + i16)])
            if Nchannels == 2:
                Y1.extend(h16temp[int(0 + i16):int(4 + i16)])
                Y2.extend(h16temp[int(4 + i16):int(8 + i16)])
                Y1.extend(h16temp[int(8 + i16):int(12 + i16)])
                Y2.extend(h16temp[int(12 + i16):int(16 + i16)])
            if Nchannels == 4:
                Y1.extend(h16temp[int(0 + i16):int(4 + i16)])
                Y2.extend(h16temp[int(4 + i16):int(8 + i16)])
                Y3.extend(h16temp[int(8 + i16):int(12 + i16)])
                Y4.extend(h16temp[int(12 + i16):int(16 + i16)])


        print("len(Y1): ",len(Y1))

        # forming X's
        X1 = []
        X2 = []
        X3 = []
        X4 = []

        for i in range(len(Y1)):
            x = kddc * i * h
            X1.append(x)
            X2.append(x)
            X3.append(x)
            X4.append(x)

        #hiding signals
        if not self.ch1_checkBox.isChecked():
            Y1=[]
            X1=[]
        if not self.ch2_checkBox.isChecked():
            Y2=[]
            X2=[]
        if not self.ch3_checkBox.isChecked():
            Y3=[]
            X3=[]
        if not self.ch4_checkBox.isChecked():
            Y4=[]
            X4=[]

        #uncheck unused checkboxes
        if Nchannels == 1:
            self.ch2_checkBox.setChecked(0)
            self.ch3_checkBox.setChecked(0)
            self.ch4_checkBox.setChecked(0)
        if Nchannels == 2:
            self.ch3_checkBox.setChecked(0)
            self.ch4_checkBox.setChecked(0)
        if Nchannels == 3:
            self.ch4_checkBox.setChecked(0)

        #print X1
        self.mainplot1.setData(y=Y1, x=X1)
        self.mainplot2.setData(y=Y2, x=X2)
        self.mainplot3.setData(y=Y3, x=X3)
        self.mainplot4.setData(y=Y4, x=X4)

        fft = fftCalc()

        print("fft started")

        # hiding signals
        if not self.ch1_checkBox.isChecked():
            FFT1= [[],[]]
            self.fftplot1.setData(y=FFT1[0], x=FFT1[1])
        else:
            FFT1 = fft.OneChannelFFT(len(Y1), Y1, X1, windowFFT=False, log10view=False)
            self.fftplot1.setData(y=FFT1[0], x=FFT1[1])
        if not self.ch2_checkBox.isChecked():
            FFT2 = [[],[]]
            self.fftplot2.setData(y=FFT2[0], x=FFT2[1])
        else:
            FFT2 = fft.OneChannelFFT(len(Y2), Y2, X2, windowFFT=False, log10view=False)
            self.fftplot2.setData(y=FFT2[0], x=FFT2[1])
        if not self.ch3_checkBox.isChecked():
            FFT3 = [[],[]]
            self.fftplot3.setData(y=FFT3[0], x=FFT3[1])
        else:
            FFT3 = fft.OneChannelFFT(len(Y3), Y3, X3, windowFFT=False, log10view=False)
            self.fftplot3.setData(y=FFT3[0], x=FFT3[1])
        if not self.ch4_checkBox.isChecked():
            FFT4 = [[],[]]
            self.fftplot4.setData(y=FFT4[0], x=FFT4[1])
        else:
            FFT4 = fft.OneChannelFFT(len(Y4), Y4, X4, windowFFT=False, log10view=False)
            self.fftplot4.setData(y=FFT4[0], x=FFT4[1])

    def finishedHndlr(self):
        print("receiv thread finished")
